# Bootstrap Scripts for LHCb User Environment

## LHCb User Environment entrypoint

The script [data/LbEnv-flavour.sh](data/LbEnv-flavour.sh) (and its `csh`
counterpart) is a template for a set of scripts that are usually installed at the
root of a local installation of LHCb software.

The script [data/LbEnv-flavour](data/LbEnv-flavour) is a small script that works
on both `bash` and `tcsh`, to detects the shell used and delegate to the
matching script.

## Environment customization logic for CERN site.

The script [data/etc/cern_profile.sh](data/etc/cern_profile.sh) (and its `csh`
counterpart) are used to provide customization of the user environment for CERN.
It is meant to be the main entry point (for HEPIX), and delegate to the files
in `cern_profile.d` for the basic environment, and to `LbEnv-flavour.sh` for
the rest.

The user can control the behaviour of the script by means of special files
in the home directory:
- `.nogrouplogin`: if present, completely disable customization
- `.nolbenv`: if present, disable LbEnv, but keep site specific variables
- `.lbenv_flavour`: it must contain any of _stable_, _testing_, _unstable_
  or _legacy_ to select the version of LbEnv kit to use. If missing or invalid,
  it is equivalent to _stable_. The flavour _legacy_ uses `LbLogin` instead of
  `LbEnv`.

For backward compatibility we also accept:
- `.noLHCBLoginscript`: same as `.nogrouplogin` (temporarily disabled until
  the new scripts will be commissioned)
- `.devLHCBLoginscript`: if present, equivalent to `.lbenv_flavour` containing
  _testing_


## RPM

In this gitlab project we build an RPM with the scripts
- `LbEnv-<flavour>`, `LbEnv-<flavour>.sh`, `LbEnv-<flavour>.csh`
  (for `<flavour>` in _stable_, _testing_ and _unstable_)
- `LbEnv`, `LbEnv.sh`, `LbEnv.csh` (shortcuts for flavour _stable_)
- `cern_profile.sh`, `cern_profile.csh`

The `LbEnv*` scripts are installed directly in `$MYSITEROOT`, while the
`cern_profile.*` once in `$MYSITEROOT/etc`.
