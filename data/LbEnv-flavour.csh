#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2018 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if ( ! $?LBENV_SOURCED ) then
  setenv LBENV_ENTRYPOINT %target_dir%/LbEnv-%flavour%.csh
  setenv LBENV_FLAVOUR %flavour%
  set _host_os=`%target_dir%/bin/%host_os_script_fn%`

  if ( -e %lbenv_root%/$_host_os/bin/activate.csh ) then
    if ( $?PYTHONPATH ) then
      # strip LBSCRIPTS entries from PYTHONPATH because they would interfere with virtualenv
      setenv PYTHONPATH `printenv PYTHONPATH | sed 's|[^:]\+/LBSCRIPTS/[^:]\+||g;s/::\+/:/g;s/:$//;s/^://'`
    endif
    source %lbenv_root%/$_host_os/bin/activate.csh
    if ( `history 1 | wc -l` ) then
      eval `python -I -m LbEnv --csh --siteroot %siteroot% !:2* || echo deactivate`
    else
      eval `python -I -m LbEnv --csh --siteroot %siteroot% || echo deactivate`
    endif
  else
    echo "Platform not supported ($_host_os)"
  endif
  
  # Prepare to send telemetry
  if ( $?OVERRIDE_LBENVROOT ) then
    set _is_override=true
    set _build_num=unknown
  else
    set _is_override=false
    set lbenv_prefix_parent=`dirname "$LBENV_PREFIX"`
    set lbenv_prefix_grandparent=`dirname "$lbenv_prefix_parent"`
    set _build_num=`basename "$lbenv_prefix_grandparent"`
  endif

  set _current_shell=`readlink "/proc/$$/exe"`
  set _payload='{"build_num": "'"$_build_num"'", "host_os": "'"$_host_os"'", "flavour": "'"$LBENV_FLAVOUR"'", "override_lbenvroot": "'"$_is_override"'", "lbenv_prefix": "'"$LBENV_PREFIX"'", "shell": "'"$_current_shell"'"}'
  lb-telemetry send "$_payload" --table lbenv_activation --tags host_os flavour override_lbenvroot build_num shell --include-host-info >& /dev/null || true

  unset _host_os _build_num _payload _is_override _current_shell lbenv_prefix_parent lbenv_prefix_grandparent
else
  if ( $?LBENV_ALIASES ) then
    source "$LBENV_ALIASES"
  endif
endif
