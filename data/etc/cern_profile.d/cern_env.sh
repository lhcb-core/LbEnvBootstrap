#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# CERN specific environment
export LHCBDEV=/afs/cern.ch/lhcb/software/DEV
export LHCBDOC=/eos/project/l/lhcbwebsites/www/projects
export LHCBNIGHTLIES=/cvmfs/lhcbdev.cern.ch/nightlies
export LBTELEMETRY_ENABLED=x

# For grid usage
if [ ! -n "$X509_CERT_DIR" ] ; then
  if [ -d /etc/grid-security/certificates ] ; then
    export X509_CERT_DIR=/etc/grid-security/certificates
  elif [ -d /cvmfs/lhcb.cern.ch/etc/grid-security/certificates ] ; then
    export X509_CERT_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/certificates
  fi
fi

if [ ! -n "$X509_VOMS_DIR" ] ; then
  if [ -d /etc/grid-security/vomsdir ] ; then
    export X509_VOMS_DIR=/etc/grid-security/vomsdir
  elif [ -d /cvmfs/lhcb.cern.ch/etc/grid-security/vomsdir ] ; then
    export X509_VOMS_DIR=/cvmfs/lhcb.cern.ch/etc/grid-security/vomsdir
  fi
fi

if [ ! -n "$X509_VOMSES" ] ; then
  if [ -d /etc/vomses ] ; then
    export X509_VOMSES=/etc/vomses
  elif [ -d /cvmfs/lhcb.cern.ch/etc/grid-security/vomses ] ; then
    export X509_VOMSES=/cvmfs/lhcb.cern.ch/etc/grid-security/vomses
  fi
fi

# Variable to access the LHCb CASTOR instance
export STAGE_HOST=castorlhcb.cern.ch

# LHCb Emacs mode
export EMACSDIR=/cvmfs/lhcb.cern.ch/lib/lhcb/TOOLS/Tools/Emacs/pro

# Make sure virtualenv does not overwrite your prompt
export VIRTUAL_ENV_DISABLE_PROMPT=1
