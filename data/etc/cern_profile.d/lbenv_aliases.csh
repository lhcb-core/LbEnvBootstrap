#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# aliases to easily start an LbEnv environment when using ~/.nolbenv
alias LbEnv-stable "source %target_dir%/LbEnv-stable.sh"
alias LbEnv-testing "source %target_dir%/LbEnv-testing.sh"
alias LbEnv-unstable "source %target_dir%/LbEnv-unstable.sh"
alias LbEnv-legacy "source %target_dir%/LbEnv-legacy.sh"
alias LbEnv "source %target_dir%/LbEnv-stable.sh"
