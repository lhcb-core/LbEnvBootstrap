#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# CERN specific environment
setenv LHCBDEV /afs/cern.ch/lhcb/software/DEV
setenv LHCBDOC /eos/project/l/lhcbwebsites/www/projects
setenv LHCBNIGHTLIES /cvmfs/lhcbdev.cern.ch/nightlies
setenv LBTELEMETRY_ENABLED x

# For grid usage
if ( ! $?X509_CERT_DIR ) then
  if ( -d /etc/grid-security/certificates ) then
    setenv X509_CERT_DIR /etc/grid-security/certificates
  else if ( -d /cvmfs/lhcb.cern.ch/etc/grid-security/certificates ) then
    setenv X509_CERT_DIR /cvmfs/lhcb.cern.ch/etc/grid-security/certificates
  endif
endif

if ( ! $?X509_VOMS_DIR ) then
  if ( -d /etc/grid-security/vomsdir ) then
    setenv X509_VOMS_DIR /etc/grid-security/vomsdir
  else if ( -d /cvmfs/lhcb.cern.ch/etc/grid-security/vomsdir ) then
    setenv X509_VOMS_DIR /cvmfs/lhcb.cern.ch/etc/grid-security/vomsdir
  endif
endif

if ( ! $?X509_VOMSES ) then
  if ( -d /etc/vomses ) then
    setenv X509_VOMSES /etc/vomses
  else if ( -d /cvmfs/lhcb.cern.ch/etc/grid-security/vomses ) then
    setenv X509_VOMSES /cvmfs/lhcb.cern.ch/etc/grid-security/vomses
  endif
endif

# Variable to access the LHCb CASTOR instance
setenv STAGE_HOST castorlhcb.cern.ch

# LHCb Emacs mode
setenv EMACSDIR /cvmfs/lhcb.cern.ch/lib/lhcb/TOOLS/Tools/Emacs/pro

# Make sure virtualenv does not overwrite your prompt
setenv VIRTUAL_ENV_DISABLE_PROMPT 1
