#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#.md
# # Environment customization logic for CERN site.
#
# By default this script sets site specific environment variables and enable
# the LHCb User Environment (LbEnv) toolkit (e.g. lb-run, lb-dev).
#
# The user can control the behaviour of the script by means of special files
# in the home directory:
# - `.nogrouplogin`: if present, completely disable customization
# - `.nolbenv`: if present, disable LbEnv, but keep site specific variables
# - `.lbenv_flavour`: it must contain any of _stable_, _testing_ or _unstable_
#   to select the version of LbEnv kit to use. If missing or invalid, it is
#   equivalent to _stable_
#
# For backward compatibility we also accept:
# - `.noLHCBLoginscript`: same as `.nogrouplogin` (temporarily disabled until
#   this script will be commissioned)
# - `.devLHCBLoginscript`: if present, equivalent to `.lbenv_flavour` containing
#   _testing_
#
_home_dir="${TEST_HOME:-${HOME}}"
if [ -e "${_home_dir}" -a ! \( -e "${_home_dir}/.nogrouplogin" \) ] ; then
  # customization enabled
  if [ -d %target_dir%/etc/cern_profile.d ] ; then
    for _f in $(ls %target_dir%/etc/cern_profile.d/ | grep '\.sh$') ; do
      source %target_dir%/etc/cern_profile.d/${_f}
    done
  fi

  if [ ! -e "${_home_dir}/.nolbenv" ] ; then
    # LbEnv enabled
    # - make sure we do not get spurious "source" command line arguments to the scripts
    shift $#
    # - check the requested flavour
    if [ -e "${_home_dir}/.devLHCBLoginscript" ] ; then
      # this is legacy for...
      _lbenv_flavour=testing
    else
      # this is the default
      _lbenv_flavour=stable
    fi
    if [ -e "${_home_dir}/.lbenv_flavour" ] ; then
      # check the content of the special file
      _lbenv_flavour=$(cat "${_home_dir}/.lbenv_flavour")
      case "${_lbenv_flavour}" in
        stable|testing|unstable|legacy) ;; # these are fine, nothing to do
        *) _lbenv_flavour=stable ;; # anything else maps to stable
      esac
    fi
    if [ "${_lbenv_flavour}" = legacy ] ; then
      if [ -z "$LBLOGIN_SOURCED" -a -e ${OVERRIDE_MYSITEROOT:-%target_dir%}/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh ] ; then
        if [[ $- == *i* ]] ; then
          # interactive shell: print banner
          source ${OVERRIDE_MYSITEROOT:-%target_dir%}/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh
        else
          # non-interactive: do not print
          source ${OVERRIDE_MYSITEROOT:-%target_dir%}/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.sh >/dev/null 2>&1
        fi
        export LBLOGIN_SOURCED=1
      fi
    else
      # source the LbEnv main script
      if [ -e %target_dir%/LbEnv-${_lbenv_flavour}.sh ] ; then
        source %target_dir%/LbEnv-${_lbenv_flavour}.sh
      fi
    fi
  fi
fi
unset _home_dir
