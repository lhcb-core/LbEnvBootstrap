#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# see cern_profile.sh for documentation
if ( $?TEST_HOME ) then
  set _home_dir="${TEST_HOME}"
else
  if ( $?HOME ) then
    set _home_dir="${HOME}"
  else
    set _home_dir=""
  endif
endif
if ( -e "${_home_dir}" && ! -e "${_home_dir}/.nogrouplogin" ) then
  # customization enabled
  if ( -d  %target_dir%/etc/cern_profile.d ) then
    foreach _f ( `ls %target_dir%/etc/cern_profile.d/ | grep '\.csh$'` )
      source %target_dir%/etc/cern_profile.d/${_f}
    end
  endif
  if ( ! -e "${_home_dir}/.nolbenv" ) then
    # LbEnv enabled
    # - check the requested flavour
    if ( -e "${_home_dir}/.devLHCBLoginscript" ) then
      # this is legacy for...
      set _lbenv_flavour=testing
    else
      # this is the default
      set _lbenv_flavour=stable
    endif
    if ( -e "${_home_dir}/.lbenv_flavour" ) then
      # check the content of the special file
      set _lbenv_flavour=`cat "${_home_dir}/.lbenv_flavour"`
      switch ("${_lbenv_flavour}")
        case stable:
        case testing:
        case unstable:
        case legacy:
          breaksw # these are fine, nothing to do
        default:
          # anything else maps to stable
          set _lbenv_flavour=stable
          breaksw
      endsw
    endif
    if ( "$_lbenv_flavour" == legacy ) then
      if ( ! $?LBLOGIN_SOURCED && -e %target_dir%/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.csh ) then
        if ( $?prompt ) then
          # interactive shell: print banner
          source %target_dir%/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.csh
        else
          # non-interactive: do not print
          source %target_dir%/lhcb/LBSCRIPTS/prod/InstallArea/scripts/LbLogin.csh >&/dev/null
        endif
        setenv LBLOGIN_SOURCED 1
      endif
    else
      # source the LbEnv main script
      if ( -e %target_dir%/LbEnv-${_lbenv_flavour}.csh ) then
        source %target_dir%/LbEnv-${_lbenv_flavour}.csh
      endif
    endif
  endif
endif
unset _home_dir
