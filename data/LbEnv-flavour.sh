#!/usr/bin/printf you must "source %s"\n
###############################################################################
# (c) Copyright 2018 CERN                                                     #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if [ -z "$LBENV_SOURCED" ] ; then
  export LBENV_ENTRYPOINT=%target_dir%/LbEnv-%flavour%.sh
  export LBENV_FLAVOUR=%flavour%
  _host_os=$(%target_dir%/bin/%host_os_script_fn%)

  if [ -e ${OVERRIDE_LBENVROOT:-%lbenv_root%/$_host_os}/bin/activate ] ; then
    if [ -n "$PYTHONPATH" ] ; then
      # strip LBSCRIPTS entries from PYTHONPATH because they would interfere with virtualenv
      export PYTHONPATH=$(printenv PYTHONPATH | sed 's|[^:]\+/LBSCRIPTS/[^:]\+||g;s/::\+/:/g;s/:$//;s/^://')
    fi
    source ${OVERRIDE_LBENVROOT:-%lbenv_root%/$_host_os}/bin/activate
    eval $(python -I -m LbEnv --sh --siteroot ${OVERRIDE_MYSITEROOT:-%siteroot%} "$@" || echo deactivate)
  else
    echo "Platform not supported: cannot find ${OVERRIDE_LBENVROOT:-%lbenv_root%/$_host_os}"
  fi
  
  # Prepare to send telemetry
  if [ -n "$OVERRIDE_LBENVROOT" ]; then
    _is_override=true
    _build_num=unknown
  else
    _is_override=false
    _build_num=$(basename "$(dirname "$(dirname "$LBENV_PREFIX")")")
  fi

  _current_shell=$(readlink "/proc/$$/exe")
  _payload="{\"build_num\": \"$_build_num\", \"host_os\": \"$_host_os\", \"flavour\": \"$LBENV_FLAVOUR\", \"override_lbenvroot\": \"$_is_override\", \"lbenv_prefix\": \"$LBENV_PREFIX\", \"shell\": \"$_current_shell\"}"
  lb-telemetry send "$_payload" --table lbenv_activation --tags host_os flavour override_lbenvroot build_num shell --include-host-info 2>/dev/null >/dev/null || true

  unset _host_os _build_num _payload _is_override _current_shell
else
  if [ -n "$LBENV_ALIASES" ] ; then
    source "$LBENV_ALIASES"
  fi
fi
