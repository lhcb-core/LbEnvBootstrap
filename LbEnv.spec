%define prefix /opt/LHCbSoft

%global __os_install_post /usr/lib/rpm/check-buildroot

Name: LbEnv
# version and release values are filled by the gitlab-ci job
Version: 0
Release: 0
Vendor: LHCb
Summary: Entry scripts for LHCb login environment
License: GPLv3
URL:     https://gitlab.cern.ch/lhcb-core/%{name}
Source0: https://gitlab.cern.ch/lhcb-core/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz

Group: LHCb

BuildArch: noarch
AutoReqProv: no
Prefix: %{prefix}
Provides: /bin/sh
Provides: /bin/bash

%description
Entry scripts for LHCb login environment

%prep
%setup -q

%build

%install
mkdir -p ${RPM_BUILD_ROOT}%{prefix}/etc/cern_profile.d
install -m 0644 data/etc/cern_profile.*sh ${RPM_BUILD_ROOT}/%{prefix}/etc
install -m 0644 data/etc/cern_profile.d/*.*sh ${RPM_BUILD_ROOT}/%{prefix}/etc/cern_profile.d

mkdir -p ${RPM_BUILD_ROOT}%{prefix}/bin
install -m 0755 data/bin/* ${RPM_BUILD_ROOT}/%{prefix}/bin

for flavour in stable testing unstable ; do
  for suff in '' .sh .csh ; do
    tgt=${RPM_BUILD_ROOT}/%{prefix}/LbEnv-${flavour}${suff}
    install -m 0644 "data/LbEnv-flavour${suff}" "${tgt}"
    sed -i "s/%flavour%/${flavour}/g" "${tgt}"
    host_os_script_fn=host_os_conda
    sed -i "s/%host_os_script_fn%/${host_os_script_fn}/g" "${tgt}"
  done
done
for suff in '' .sh .csh ; do
  ln -s LbEnv-stable${suff} ${RPM_BUILD_ROOT}/%{prefix}/LbEnv${suff}
done


%clean

%post -p /bin/bash
for flavour in stable testing unstable ; do
  for suff in '' .sh .csh ; do
    tgt=${RPM_INSTALL_PREFIX}/LbEnv-${flavour}${suff}
    sed -i "s#%\(target_dir\|siteroot\)%#${RPM_INSTALL_PREFIX}#g;s#%lbenv_root%#${RPM_INSTALL_PREFIX}/var/lib/LbEnv/${flavour}#g" "${tgt}"
  done
done
for script in ${RPM_INSTALL_PREFIX}/etc/cern_profile.{sh,csh} \
              ${RPM_INSTALL_PREFIX}/etc/cern_profile.d/{cern_env,lbenv_aliases}.{sh,csh} ; do
  sed -i "s#%target_dir%#${RPM_INSTALL_PREFIX}#g" "${script}"
done

%postun -p /bin/bash

%files
%defattr(-,root,root)
%{prefix}/etc/cern_profile.sh
%{prefix}/etc/cern_profile.csh
%{prefix}/etc/cern_profile.d/cern_env.sh
%{prefix}/etc/cern_profile.d/cern_env.csh
%{prefix}/etc/cern_profile.d/lbenv_aliases.sh
%{prefix}/etc/cern_profile.d/lbenv_aliases.csh
%{prefix}/bin/host_os
%{prefix}/bin/host_os_conda
%{prefix}/bin/LbLogin.sh
%{prefix}/LbEnv
%{prefix}/LbEnv.sh
%{prefix}/LbEnv.csh
%{prefix}/LbEnv-stable
%{prefix}/LbEnv-stable.sh
%{prefix}/LbEnv-stable.csh
%{prefix}/LbEnv-testing
%{prefix}/LbEnv-testing.sh
%{prefix}/LbEnv-testing.csh
%{prefix}/LbEnv-unstable
%{prefix}/LbEnv-unstable.sh
%{prefix}/LbEnv-unstable.csh


%changelog
* Mon May 18 2020 Chris Burr <christopher.burr@cern.ch>
- Use conda based environment for unstable flavour
* Mon Feb 25 2015 Marco Clemencic <marco.clemencic@cern.ch>
- first version
